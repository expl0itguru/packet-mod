CC=g++
ARCH=x86
PROG=pmod
INCLS=-I$(ARCH)/libnetfilter_queue/include -I$(ARCH)/libnfnetlink/include 
DEFS=-DHAVE_CONFIG_H -D_U_="__attribute__((unused))"
CFLAGS=-static
FULL_CFLAGS=$(CCOPT) $(DEFS) $(INCLS) $(CFLAGS)
LIBS=$(ARCH)/libnetfilter_queue/src/extra/.libs/ipv6.o $(ARCH)/libnetfilter_queue/src/extra/.libs/tcp.o $(ARCH)/libnetfilter_queue/src/extra/.libs/udp.o $(ARCH)/libnetfilter_queue/src/extra/.libs/ipv4.o $(ARCH)/libnetfilter_queue/src/extra/.libs/checksum.o $(ARCH)/libnetfilter_queue/src/extra/.libs/pktbuff.o $(ARCH)/libnetfilter_queue/src/.libs/libnetfilter_queue.o $(ARCH)/libnetfilter_queue/src/.libs/nlmsg.o $(ARCH)/libnfnetlink/src/.libs/libnfnetlink.o $(ARCH)/libnfnetlink/src/.libs/iftable.o $(ARCH)/libnfnetlink/src/.libs/rtnl.o $(ARCH)/libmnl/src/.libs/libmnl.a

$(PROG):  $@
	@rm -f $@
	$(CC) $@.cpp $(FULL_CFLAGS) $(LDFLAGS) -o $@ -static $(LIBS)

	# ./$(PROG) "GET" "POST"

clean:
	@rm -f $(PROG)
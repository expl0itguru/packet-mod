#include <iostream>
#include <string>
#include <regex>
#include <array>
#include <unistd.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <linux/netfilter.h>

extern "C"
{
#include <libnetfilter_queue/pktbuff.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libnetfilter_queue/libnetfilter_queue_ipv6.h>
#include <libnetfilter_queue/libnetfilter_queue_tcp.h>
}

#define THROW_IF_TRUE(x, m) do { if((x)) { throw std::runtime_error(m); }} while(false)

std::regex find ("");
std::string replace = "";

static int callback(struct nfq_q_handle *queue, struct nfgenmsg *nfmsg, struct nfq_data *nfad, void *data) {    
    // Add back to assist with debugging:
    // printf("Callback hit\n");
    
    struct nfqnl_msg_packet_hdr *ph = nfq_get_msg_packet_hdr(nfad);
    THROW_IF_TRUE(ph == nullptr, "Issue with packet header.");
      
    u_char *rawData = nullptr;
    int len = nfq_get_payload(nfad, &rawData);
    THROW_IF_TRUE(len < 0, "Payload length less than 0?");
  
    // 0x1000 (4096) extra bytes to play with
    struct pkt_buff * pkBuff = pktb_alloc(AF_INET6, rawData, len, 0x1000);
    THROW_IF_TRUE(pkBuff == nullptr, "Could not allocate memory.");

    if (!pktb_mangled(pkBuff)) {
        struct ip6_hdr *ip = nfq_ip6_get_hdr(pkBuff);
        THROW_IF_TRUE(ip == nullptr, "Failed to get IP header.");
    
        THROW_IF_TRUE(nfq_ip6_set_transport_header(pkBuff, ip, IPPROTO_TCP) < 0, "Cannot set transport header.");
        
        struct tcphdr *tcp = nfq_tcp_get_hdr(pkBuff);
        THROW_IF_TRUE(tcp == nullptr, "Unanle to get TCP header.");
                    
        void *payload = nfq_tcp_get_payload(tcp, pkBuff);
        u_short payloadLen = nfq_tcp_get_payload_len(tcp, pkBuff);
        THROW_IF_TRUE(payload == nullptr, "Could not get payload pointer.");

        // Build an operable string
        std::string tmp = "";
        
        // Encode nulls and backslashes
        for (u_short i = 0; i < payloadLen; i++) {
            char byte = ((char *)payload)[i];

            if (byte == '\0') {
                tmp += "\\0";
            } else if (byte == '\\') {
                tmp += "\\\\";
            } else {
                tmp += byte;
            }
        }

        // Do regex and replace payload data
        tmp = std::regex_replace(tmp.c_str(), find, replace);

        // Bad fix
        for (u_short i; i < payloadLen; i++) {
            ((char *)payload)[i] = '\0';
        }

        // Decode after performing regex on human-readable data
        u_short newLen = 0;
        for (u_short i = 0; i < tmp.length(); i++) {
            if (tmp[i] == '\\' && tmp[i + 1] == '0') {
                ((char *)payload)[newLen++] = '\0';
                i++;
            
            } else if (tmp[i] == '\\' && tmp[i + 1] == '\\') {
                ((char *)payload)[newLen++] = '\\';
                i++;

            } else {
                ((char *)payload)[newLen++] = tmp[i];
            }        
        }

        // Update payload length - checksum is taken care of automatically
        nfq_tcp_mangle_ipv6(pkBuff, 0, payloadLen, (char *)payload, newLen);
        
        return nfq_set_verdict(queue, ntohl(ph->packet_id), NF_ACCEPT, pktb_len(pkBuff), pktb_data(pkBuff));
    }

    return nfq_set_verdict(queue, ntohl(ph->packet_id), NF_ACCEPT, pktb_len(pkBuff), pktb_data(pkBuff));
}


int main(int argc, char **argv) {
    // Check if sufficient arguments were supplied
    if (argc != 3) {
        printf("Usage: %s [find regex] [replace regex]\n", argv[0]);
        printf("E.g. %s \"POST\" \"GET\"", argv[0]);
        return 0;
    }

    // Regex strings
    find = (argv[1]);
    replace = (argv[2]);
    
    struct nfq_handle *handler = nfq_open();
    THROW_IF_TRUE(handler == nullptr, "Cannot open hfqueue handler.");
    
    struct nfq_q_handle *queue = nfq_create_queue(handler, 0, callback, nullptr);
    THROW_IF_TRUE(queue == nullptr, "Cannot create queue handler.");
    
    // NFQNL_COPY_NONE, NFQNL_COPY_META or NFQNL_COPY_PACKET - capture complete packets (65535 bytes)
    THROW_IF_TRUE(nfq_set_mode(queue, NFQNL_COPY_PACKET, 0xffff) < 0, "Cannot set queue copy mode.");
  
    int fd = nfq_fd(handler);

    // Buffer size is the maximum size of a packet*
    std::array<char, 0x10000> buffer;
    for(;;) {
        int len = read(fd, buffer.data(), buffer.size());       
        THROW_IF_TRUE(len < 0, "Read error.");

        nfq_handle_packet(handler, buffer.data(), len);
    }

    return 0;
}
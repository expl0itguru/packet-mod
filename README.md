# Packet Mod
Packet Mod (or `pmod` for short) is a tool which allows interception and modification of packets in transit with regular expression. Currently, only IPv6 TCP packets are intercepted. All interfaces are supported, including loopback interfaces (`127.0.0.1/8` and `::1/128`).



# Building The Project
##### Initial Setup:
```shell
$ git clone https://gitlab.com/joseph.foote/packet-mod
$ cd packetmod
```



##### Building `libmnl`:
```shell
$ git clone https://git.netfilter.org/libmnl
$ cd libmnl
$ ./autoconf.sh
$ ./configure
$ make clean
$ make LDFLAGS="-all-static"
$ sudo make install
$ cd ..
```



##### Building `libnfnetlink`:
```shell
$ git clone https://git.netfilter.org/libnfnetlink/
$ cd libnfnetlink
$ ./autoconf.sh
$ ./configure
$ make clean
$ make
$ sudo make install
$ cd ..
```



##### Building `libnetfilter_queue`:
```shell
$ git clone https://git.netfilter.org/libnetfilter_queue/
$ cd libnetfilter_queue
$ ./autoconf.sh
$ ./configure
$ make clean
$ make
$ sudo make install
$ cd ..
```



Finally, build and run Packet Mod. Note: `iptables` must be configured prior to launch. 
```shell
$ make -B
# Replaces TCP payloads containing 'HTTP' with the word 'HACK'.
$ sudo ./pmod HTTP HACK
```



# Cross-Compiling For ARM
```shell
$ sudo apt install g++-arm-linux-gnueab gcc-arm-linux-gnueabi
$ export CC=gcc-arm-linux-gnueabi-cc
$ export AR=gcc-arm-linux-gnueabi-ar
$ export LD=gcc-arm-linux-gnueabi-ld
```



Now repeat the standard compilation steps outlined above. When running `./configure`, the `--host arm-linux` flag must be appended. It's recommended to build x86 and ARM in their own sub-directories e.g.:
```
.
├── Makefile
├── pmod
├── pmod.cpp
├── README.md
├── arm
│   ├── libmnl
│   ├── libnetfilter_queue
│   └── libnfnetlink
└── x86
    ├── libmnl
    ├── libnetfilter_queue
    └── libnfnetlink
```



# Verification of Functionality
The below example sets up a `netcat` listener on IPv6 loopback to debug `curl`'s HTTP request. The protocol header has been updated from HTTP/1.1 to HACK/1.1.
```shell
$ sudo nc -6lp 80 &
$ curl -6 localhost:80
GET / HACK/1.1
Host: localhost
User-Agent: curl/7.67.0
Accept: */*
```



# Setting up iptables/ip6tables
```shell
# Save any old rules to restore if required:
$ sudo iptables-save > ipv4_tbl.bak
$ sudo ip6tables-save > ipv6_tbl.bak

$ sudo iptables-nft-save > ipv4_nft_tbl.bak
$ sudo ip6tables-nft-save > ipv6_nft_tbl.bak

# Flush existing rules to prevent interference
$ sudo iptables -t raw -F
$ sudo iptables -t nat -F
$ sudo iptables -t mangle -F
$ sudo iptables -F
$ sudo iptables -X

$ sudo ip6tables -t raw -F
$ sudo ip6tables -t nat -F
$ sudo ip6tables -t mangle -F
$ sudo ip6tables -F
$ sudo ip6tables -X

# NFQUEUE config
$ sudo ip6tables -A PREROUTING -t mangle -j NFQUEUE --queue-num 0 --queue-bypass
```



# Kernel Modules
Packet Mod leverages kernel modules provided by `iptables`. If these modules are not available for your system, you may need to manually compile them yourself and load them with:
```shell
$ modprobe -a nfnetlink nfnetlink_queue nf_tables
```



# Development Road Map
- IPv4 and UDP support.
- Safe handling of malformed packets and data.
- Command line arguments for:
  - UDP, TCP or mixed mode support, etc.
  - Parsing regex patterns and replacement strings from file.
- Multi-architecture `Makefile` to enable easier support of ARM/cross-compiling.
- Add debugging/verbose flag to confirm configuration of `iptables`.
- Change modification method to allow `libpcap` and other monitoring tools to see modified traffic.
- Creation of docker file for compiling dependencies.
- Add support for modification of header data.
- Manual retransmissions after mangling packets to prevent dumb listeners from receiving malformed data.
- Fix regex capture groups.



# FAQ
##### Iptables: prerouting or postrouting?
- PREROUTING: Immediately after been received by an interface.
- POSTROUTING: Right before leaving an interface. Using this may solve the `libpcap` capture issue.

##### How much of the packet can I modify?
- Currently, only payloads may be modified. An allocation of 4096 bytes additional bytes has been set for payload injection. You may increase this value in the source code. Dymanic allocation is not currently supported.

##### My regex isn't working or something else is broken, why?
- The tool has not been fully tested yet, so there may still be issues. Refer to the regex_* documentation for C++ to ensure format is correct prior to making a bug report.
